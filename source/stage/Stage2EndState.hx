package stage;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;

/**
 * ...
 * @author Dahamugo
 */
class Stage2EndState extends FlxState {
    private var timer: Int = 0;
    private var screen: FlxSprite;
    private var fade: FlxSprite;

    override public function create():Void {
        super.create();

        screen = new FlxSprite(0, 0);
        screen.loadGraphic(AssetPaths.Map1End__png, true, 160, 144);
        screen.animation.add("screen", [0, 1], 2, true);
        screen.animation.play("screen");
        add(screen);

        fade = new FlxSprite(0, 0);
        fade.makeGraphic(160, 144);
        add(fade);

        timer = 200;
    }

    override public function update(elapsed: Float):Void {
        super.update(elapsed);

        if (timer > 0) {
            timer -= 2;
            fade.alpha = (Math.floor(timer / 33) * 33 + 1) / 100;
        } else {
            if (FlxG.keys.justPressed.ENTER) {
                FlxG.sound.music.stop();
                FlxG.switchState(new Stage1IntroState());
            }
        }
    }

}
