package stage;

import flixel.FlxSprite;

/**
 * ...
 * @author Dahamugo
 */

enum EnemyFacing {
    LEFT;
    RIGHT;
}

enum EnemyState {
    ORIGINAL;
    COLORED;
}

class Stage1Enemy extends FlxSprite {
    private static inline var ENEMY_SPEED = 5/8;

    private var face: EnemyFacing;
    private var state: EnemyState = ORIGINAL;
    private var leftmost: Float;
    private var rightmost: Float;

    public function new(_x:Float=0, _y:Float=0, initial: String, leftrange: Int, rightrange: Int) {
        super(_x, _y);
        if (initial == "left") {
            faceLeft();
        } else {
            faceRight();
        }

        leftmost = _x - leftrange * 16;
        rightmost = _x + rightrange * 16;

		this.loadGraphic(AssetPaths.Map1Obstacle2__png, true, 16, 16);
        this.animation.add("original", [0], 1, true);
        this.animation.add("color", [1, 2, 3, 4], 5, true);
        this.animation.play("original");
    }

    public function live() {
        this.animation.play("color");
        state = COLORED;
    }

    public function faceLeft() {
        this.flipX = true;
        face = LEFT;
    }

    public function faceRight() {
        this.flipX =  false;
        face = RIGHT;
    }

    override public function update(elapsed: Float):Void {
        super.update(elapsed);

        if (state == COLORED) {

            switch(face) {
                case LEFT:
                    x -= ENEMY_SPEED;
                    if (x < leftmost) faceRight();

                case RIGHT:
                    x += ENEMY_SPEED;
                    if (x > rightmost) faceLeft();
            }
        }
    }
}
