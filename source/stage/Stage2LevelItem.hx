package stage;
import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class Stage2LevelItem extends LevelItem{

    public function new(_x:Float=0, _y:Float=0) {
        super(_x, _y);
        itemSprite = new FlxSprite();
        itemSprite.loadGraphic(AssetPaths.Item2__png, true, 32, 32);
        itemSprite.animation.add("base", [0, 1, 2, 3, 4, 5, 6, 7, 8], 10, true);
        itemSprite.animation.play("base");
        add(itemSprite);

        hitbox = new FlxSprite(14, 4);
        hitbox.makeGraphic(4, 12, 0x01FFFFFF);
        add(hitbox);

        this.visible = false;
    }
}
