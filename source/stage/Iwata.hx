package stage;

import flixel.FlxSprite;
import flixel.math.FlxPoint;

/**
 * ...
 * @author Dahamugo
 */
class Iwata extends FlxSprite {

    public var speedRatio: Float = 1;

    public function new(X:Float = 0, Y:Float = 0, stage: Int = 1) {
        super(X, Y);
        this.offset = new FlxPoint(15, 25);
        switch (stage) {
            case 1:
                this.loadGraphic(AssetPaths.Iwata1__png, true, 29, 25);

            case 2:
                this.loadGraphic(AssetPaths.Iwata2Color__png, true, 29, 25);
        }

        this.animation.add("stand", [0], 1, true);
        this.animation.add("duck", [1], 1, true);
        this.animation.add("run", [2, 3, 4], 10, true);
        this.animation.add("skid", [8], 1, true);
        this.animation.add("jump", [5], 1, true);
        this.animation.add("fall", [6], 1, true);
        this.animation.add("hit", [7], 1, true);
        this.animation.play("stand");
    }

    public function faceLeft() {
        this.flipX = true;
        this.offset = new FlxPoint(14, 25);
    }

    public function faceRight() {
        this.flipX =  false;
        this.offset = new FlxPoint(15, 25);
    }

    public function skid() {
        this.animation.play("skid");
    }

    public function run() {
        if (this.animation.curAnim.name != "run") {
            this.animation.play("run");
            this.animation.curAnim.curFrame = 0;
            this.animation.curAnim.frameRate = 5 + Math.ceil(8 * speedRatio);
        }
    }

    public function stand() {
        this.animation.play("stand");
    }

    public function duck() {
        this.animation.play("duck");
    }

    public function fall() {
        this.animation.play("fall");
    }

    public function jump() {
        this.animation.play("jump");
    }

    public function hit() {
        this.animation.play("hit");
    }

    override public function update(elapsed: Float):Void {
        super.update(elapsed);

        if (this.animation.curAnim.name == "run") {
            this.animation.curAnim.frameRate = 5 + Math.ceil(8 * speedRatio);
        }
    }

    public function live() {
        var curAnim = this.animation.curAnim.name;
        var curFrame = this.animation.curAnim.curFrame;
        this.loadGraphic(AssetPaths.Iwata1Color__png, true, 29, 25);
        this.animation.add("stand", [0], 1, true);
        this.animation.add("duck", [1], 1, true);
        this.animation.add("run", [2, 3, 4], 10, true);
        this.animation.add("skid", [8], 1, true);
        this.animation.add("jump", [5], 1, true);
        this.animation.add("fall", [6], 1, true);
        this.animation.add("hit", [7], 1, true);
        this.animation.play(curAnim, true, false, curFrame);
    }
}
