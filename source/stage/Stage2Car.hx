package stage;
import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class Stage2Car extends FlxSprite {
    private static inline var MINIMUM_SPEED = 24 / 16;
    private static inline var MAXIMUM_SPEED = 50 / 16;

    public var speed: Float;

    public function new(_x:Float = 0, _y:Float = 0, flip: Bool) {
        super(_x, _y);

        this.speed = MINIMUM_SPEED + Math.random() * (MAXIMUM_SPEED - MINIMUM_SPEED);

        if (flip) {
            this.loadGraphic(AssetPaths.Map2CarLeft__png);
        } else {
            this.loadGraphic(AssetPaths.Map2CarRight__png);
        }
    }

}
