package stage;

import addons.tiled.TiledLayer;
import flixel.FlxCamera;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.FlxG;
import flixel.math.FlxRect;
import flixel.system.FlxSound;
import flixel.math.FlxAngle;
import flixel.util.FlxColor;
import flixel.math.FlxPoint;

/**
 * ...
 * @author Dahamugo
 */
class Stage1State extends StageState {
    private var mushrooms: Array<Stage1Mushroom> = [];
    private var enemies: Array<Stage1Enemy> = [];
    private var collectibles: Array<Stage1Collectible> = [];
    private var clouds: Array<Stage1Cloud> = [];

    private static inline var MINIMUM_SPEED = 19 / 256;
    private static inline var MAXIMUM_SPEED = 25 / 16;
    //private static inline var ACCELERATION = 19 / 512;
    private static inline var ACCELERATION = 37 / 512;
    private static inline var NATURAL_DECELERATION = 13 / 256;
    private static inline var OPPOSITE_DECELERATION = 13 / 128;
    private static inline var SKID_THRESHOLD = 9 / 16;
    //private static inline var AIR_ACCELERATION = 19 / 512;
    private static inline var AIR_ACCELERATION = 26 / 512;
    private static inline var DROP_GRAVITY = 5 / 32;
    private static inline var REGULAR_GRAVITY = 7 / 16;
    private static inline var FAST_REGULAR_GRAVITY = 6 / 16;
    private static inline var HOLD_GRAVITY = 3 / 16;
    private static inline var FAST_HOLD_GRAVITY = 47 / 256;
    private static inline var JUMP_SPEED = -4;
    private static inline var FAST_GRAVITY_THRESHOLD = 1;

    override public function create():Void {

        mapLayer = new FlxGroup();
        objectLayer = new FlxGroup();
        buttonLayer = new FlxGroup();
        add(mapLayer);
        add(buttonLayer);
        add(objectLayer);

        _x = 20;
        _y = 80;
        iwata = new Iwata(_x, _y);
        objectLayer.add(iwata);

        cameraFocus = new FlxSprite(20, 80);
        cameraFocus.makeGraphic(1, 1, FlxColor.TRANSPARENT);
        objectLayer.add(cameraFocus);

        FlxG.camera.follow(cameraFocus, FlxCameraFollowStyle.NO_DEAD_ZONE);
        FlxG.camera.deadzone = new FlxRect(FlxG.camera.width / 2, FlxG.camera.height / 2, 1, 1);

        currentMap = new GameMap("assets/Map1/Map1.tmx", "Map1TilesOrign", "assets/Map1/");
        for (tilemap in currentMap.tilemapList) {
            tilemap.follow(FlxG.camera);
            mapLayer.add(tilemap);
        }

        currentMap.loadStage1Objects(this);

        fadeCover = new FlxSprite(0, 0);
        fadeCover.makeGraphic(160, 144, FlxColor.WHITE);
        fadeCover.alpha = 0;
        fadeCover.scrollFactor.x = fadeCover.scrollFactor.y = 0;
        add(fadeCover);

        var hud = new FlxSprite(0, 135, AssetPaths.Map1HUD__png);
        hud.scrollFactor.x = hud.scrollFactor.y = 0;
        add(hud);

        endFadeCover = new FlxSprite(0, 0);
        endFadeCover.makeGraphic(160, 144, FlxColor.WHITE);
        endFadeCover.alpha = 0;
        endFadeCover.scrollFactor.x = endFadeCover.scrollFactor.y = 0;
        add(endFadeCover);

        _jumpSound = FlxG.sound.load(AssetPaths.Jump__wav);
        _obstacleSound = FlxG.sound.load(AssetPaths.Obstacle__wav);
        _collectibleSound = FlxG.sound.load(AssetPaths.Map1CollectGrab__wav);
        _buttonSound = FlxG.sound.load(AssetPaths.LifebuttonPress__wav);
        _buttonFadeSound = FlxG.sound.load(AssetPaths.LifebuttonFade2__wav);
        FlxG.sound.playMusic(AssetPaths.BGM1__wav, 0.75, true);
    }

    override function checkCollision() {
        if (--hitstun >= 0) return;

        for (mushroom in mushrooms) {
            if (Math.abs(_x - mushroom.x) > 64 || Math.abs(_y - mushroom.y) > 64) {}
            else if (FlxG.pixelPerfectOverlap(iwata, mushroom)) {
                var angle = (new FlxPoint(_x, _y).angleBetween(new FlxPoint(mushroom.x + 8, mushroom.y + 16)) + 90) * FlxAngle.TO_RAD;

                var minXSpeed: Float = 0;
                var minYSpeed: Float = 0;
                if (Math.cos(angle) < 0) {
                    iwata.faceRight();
                    minXSpeed = -3/5;
                } else if (Math.cos(angle) > 0) {
                    iwata.faceLeft();
                    minXSpeed = 3/5;
                }
                if (_y > (mushroom.y + 16)) {
                    minYSpeed = 3;
                } else {
                    minYSpeed = -3;
                }

                _xspeed = (Math.abs(_xspeed) * 1/2 + 1/2) * Math.cos(angle) + minXSpeed;
                _yspeed = (Math.abs(_yspeed) * 1 / 2) * Math.sin(angle) +minYSpeed;

                //mushroom.live();

                iwata.hit();
                iwataState = HIT;
                hitstun = 5;
                _gravity = REGULAR_GRAVITY;
                _obstacleSound.play(true);
            }
        }


        for (enemy in enemies) {
            if (Math.abs(_x - enemy.x) > 64 || Math.abs(_y - enemy.y) > 64) {}
            else if (FlxG.pixelPerfectOverlap(iwata, enemy)) {
                var angle = (new FlxPoint(_x, _y).angleBetween(new FlxPoint(enemy.x + 8, enemy.y + 16)) + 90) * FlxAngle.TO_RAD;

                var minXSpeed: Float = 0;
                var minYSpeed: Float = 0;
                if (Math.cos(angle) < 0) {
                    iwata.faceRight();
                    minXSpeed = -3/5;
                } else if (Math.cos(angle) > 0) {
                    iwata.faceLeft();
                    minXSpeed = 3/5;
                }

                if (_y > (enemy.y + 16)) {
                    minYSpeed = 3;
                } else {
                    minYSpeed = -3;
                }

                _xspeed = (Math.abs(_xspeed) * 1/2 + 1/2) * Math.cos(angle) + minXSpeed;
                _yspeed = (Math.abs(_yspeed) * 1 / 2) * Math.sin(angle) +minYSpeed;

                //enemy.live();

                iwata.hit();
                iwataState = HIT;
                hitstun = 5;
                _gravity = REGULAR_GRAVITY;
                _obstacleSound.play(true);
            }
        }

        for (collectible in collectibles) {
            if (Math.abs(_x - collectible.x) > 64 || Math.abs(_y - collectible.y) > 64) {}
            else if (FlxG.pixelPerfectOverlap(iwata, collectible)) {
                if (collectible.collect()) {
                    _collectibleSound.play(true);
                }
            }
        }

        super.checkCollision();
    }


    public function addMushroom(tileX: Int, tileY: Int) {
        var mushroom = new Stage1Mushroom(tileX, tileY);
        objectLayer.add(mushroom);
        mushrooms.push(mushroom);
    }

    public function addEnemy(tileX: Int, tileY: Int, initial: String, leftrange: Int, rightrange: Int) {
        var enemy = new Stage1Enemy(tileX, tileY, initial, leftrange, rightrange);
        objectLayer.add(enemy);
        enemies.push(enemy);
    }

    public function addCollectible(tileX: Int, tileY: Int) {
        var collectible = new Stage1Collectible(tileX, tileY);
        objectLayer.add(collectible);
        collectibles.push(collectible);
    }

    public function addButton(tileX: Int, tileY: Int) {
        lifeButton = new LifeButton(tileX, tileY);
        buttonLayer.add(lifeButton);
    }

    public function addCloud(tileX: Int, tileY: Int, type: Int) {
        var cloud = new Stage1Cloud(tileX, tileY, type);
        mapLayer.add(cloud);
        clouds.push(cloud);
    }

    public function addLevelItem(tileX: Int, tileY: Int) {
        levelItem = new Stage1LevelItem(tileX, tileY);
        objectLayer.add(levelItem);
    }

    override function updateTransition () {
        if (fadeType == "button") {
            if (fadingIn) {
                fadeCover.visible = true;
                fadeLevel = fadeLevel + 2 > 150 ? 150 : fadeLevel + 2;
                fadeCover.alpha = (Math.ceil(fadeLevel / 33) * 33 + 1) / 100;
                if (fadeLevel == 20) _buttonFadeSound.play();
                if (fadeLevel == 150) {
                    fadingIn = false;
                    for (mushroom in mushrooms) {
                        mushroom.live();
                    }
                    for (enemy in enemies) {
                        enemy.live();
                    }
                    for (collectible in collectibles) {
                        collectible.live();
                    }
                    for (cloud in clouds) {
                        cloud.live();
                    }
                    lifeButton.live();
                    levelItem.live();
                    iwata.live();
                    for (tilemap in currentMap.tilemapList) {
                        mapLayer.remove(tilemap);
                    }
                    currentMap.live("Map1TilesColor");
                    for (tilemap in currentMap.tilemapList) {
                        mapLayer.add(tilemap);
                    }
                }
            } else {
                fadeLevel = fadeLevel - 2 < 0 ? 0 : fadeLevel - 2;
                fadeCover.alpha = (Math.floor(fadeLevel / 33) * 33 + 1) / 100;
                if (fadeLevel == 0) {
                    worldState = COLORED;
                    FlxG.sound.playMusic(AssetPaths.BGM2__wav, 0.75, true);
                }
            }
        }
        else if (fadeType == "item") {
            if (fadingIn) {
                endFadeCover.visible = true;
                fadeLevel = fadeLevel + 2 > 150 ? 150 : fadeLevel + 2;
                endFadeCover.alpha = (Math.ceil(fadeLevel / 33) * 33 + 1) / 100;
                if (fadeLevel == 20) _buttonFadeSound.play();
                if (fadeLevel == 150) {
                    FlxG.switchState(new Stage1EndState());
                }
            }
        }
    }
}
