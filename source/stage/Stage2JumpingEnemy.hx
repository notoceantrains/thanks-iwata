package stage;

import flixel.FlxSprite;

/**
 * ...
 * @author Dahamugo
 */

enum JumpingEnemyState {
    ORIGINAL;
    COLORED;
}

class Stage2JumpingEnemy extends FlxSprite {
    private static inline var JUMP_SPEED = -3;
    private static inline var HOLD_GRAVITY = 3 / 16;
    private static inline var REGULAR_GRAVITY = 7 / 16;

    public var state: JumpingEnemyState = ORIGINAL;
    private var originalY: Float;
    private var yDelta: Float = 0;
    public var ySpeed: Float = 0;
    private var jumpDelay: Int;

    public function new(_x:Float=0, _y:Float=0, sprite: Int) {
        super(_x, _y);

        originalY = _y;

        if (sprite == 1) {
            this.loadGraphic(AssetPaths.Map2Obstacle1__png, true, 16, 32);
        } else {
            this.loadGraphic(AssetPaths.Map2Obstacle2__png, true, 16, 32);
        }

        this.animation.add("original", [0], 1, true);
        this.animation.add("color", [1, 2], 3, true);
    }

    public function live() {
        if (state == COLORED)
            return;

        this.animation.play("color");
        jumpDelay = 45;
        state = COLORED;
    }

    public function jump() {
        if (yDelta < 0)
            return;

        this.jumpDelay = 0;
        this.animation.pause();
        this.ySpeed = JUMP_SPEED;
    }

    override public function update(elapsed: Float):Void {
        super.update(elapsed);

        if (state == COLORED) {
            if (jumpDelay > 0) {
                jumpDelay--;

                if (jumpDelay == 0) {
                    this.jump();
                }
                return;
            }

            yDelta += ySpeed;

            if (ySpeed < 0) {
                ySpeed += HOLD_GRAVITY;
            } else {
                ySpeed += REGULAR_GRAVITY;
            }

            this.y = originalY + yDelta;

            if (yDelta >= 0) {
                yDelta = 0;
                ySpeed = 0;
                jumpDelay = 45;
                this.animation.resume();
            }
        }
    }
}
