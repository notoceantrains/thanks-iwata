package stage;

import addons.tiled.TiledLayer;
import flixel.FlxCamera;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.FlxG;
import flixel.system.FlxSound;
import flixel.math.FlxAngle;
import flixel.util.FlxColor;
import flixel.math.FlxPoint;

enum IwataState {
    GROUND;
    AIR;
    HIT;
}

enum WorldState {
    GREY;
    TRANSITIONING;
    COLORED;
}

/**
 * ...
 * @author Dahamugo
 */
class StageState extends FlxState {
    private var worldState: WorldState = GREY;
    private var fadingIn: Bool = true;
    private var fadeLevel: Int = 0;
    private var fadeType: String;

    public var mapLayer: FlxGroup;
    public var objectLayer: FlxGroup;
    public var buttonLayer: FlxGroup;
    public var fadeCover: FlxSprite;
    public var endFadeCover: FlxSprite;

    private var iwata: Iwata;
    private var _x: Float = 0;
    private var _y: Float = 0;
    private var _xspeed: Float = 0;
    private var _yspeed: Float = 0;
    private var _gravity: Float = DROP_GRAVITY;
    private var hitstun: Int = 0;
    private var lifeButton: LifeButton;
    private var levelItem: stage.LevelItem;

    var hitboxW: Int = 14;
    var hitboxH: Int = 20;

    private var cameraFocus: FlxSprite;
    private var currentMap: GameMap;

    private static inline var MINIMUM_SPEED = 19 / 256;
    private static inline var MAXIMUM_SPEED = 25 / 16;
    //private static inline var ACCELERATION = 19 / 512;
    private static inline var ACCELERATION = 37 / 512;
    private static inline var NATURAL_DECELERATION = 13 / 256;
    private static inline var OPPOSITE_DECELERATION = 13 / 128;
    private static inline var SKID_THRESHOLD = 9 / 16;
    //private static inline var AIR_ACCELERATION = 19 / 512;
    private static inline var AIR_ACCELERATION = 26 / 512;
    private static inline var DROP_GRAVITY = 5 / 32;
    private static inline var REGULAR_GRAVITY = 7 / 16;
    private static inline var FAST_REGULAR_GRAVITY = 6 / 16;
    private static inline var HOLD_GRAVITY = 3 / 16;
    private static inline var FAST_HOLD_GRAVITY = 47 / 256;
    private static inline var JUMP_SPEED = -4;
    private static inline var FAST_GRAVITY_THRESHOLD = 1;

    private var iwataState: IwataState = AIR;

    private var _jumpSound: FlxSound;
    private var _obstacleSound: FlxSound;
    private var _collectibleSound: FlxSound;
    private var _buttonSound: FlxSound;
    private var _buttonFadeSound: FlxSound;

    override public function update(elapsed: Float):Void {
        if (worldState != TRANSITIONING) {
            handleMovement();
            updatePosition();
            checkCollision();
            super.update(elapsed);
        } else {
            updateTransition();
        }
    }

    function handleMovement() {
        if (iwataState == GROUND) {
            if (FlxG.keys.pressed.DOWN) {
                iwata.duck();
                if (_xspeed > 0) {
                    _xspeed -= Math.min(_xspeed, NATURAL_DECELERATION);
                } else if (_xspeed < 0) {
                    _xspeed += Math.min( -_xspeed, NATURAL_DECELERATION);
                }
            } else if (FlxG.keys.pressed.RIGHT && !FlxG.keys.pressed.LEFT) {
                if (_xspeed >= 0) {
                    iwata.faceRight();
                    if (_xspeed < MINIMUM_SPEED) _xspeed = MINIMUM_SPEED;
                    else _xspeed += ACCELERATION;
                    iwata.run();
                } else {
                    if (_xspeed <= -SKID_THRESHOLD) {
                        iwata.faceRight();
                        iwata.skid();
                    }
                    _xspeed += OPPOSITE_DECELERATION;
                }
            } else if (FlxG.keys.pressed.LEFT && !FlxG.keys.pressed.RIGHT) {
                if (_xspeed <= 0) {
                    iwata.faceLeft();
                    if (_xspeed > -MINIMUM_SPEED) _xspeed = -MINIMUM_SPEED;
                    _xspeed -= ACCELERATION;
                    iwata.run();
                } else {
                    if (_xspeed >= SKID_THRESHOLD) {
                        iwata.faceLeft();
                        iwata.skid();
                    }
                    _xspeed -= OPPOSITE_DECELERATION;
                }
            } else {
                if (_xspeed > 0) {
                    _xspeed -= Math.min(_xspeed, NATURAL_DECELERATION);
                } else if (_xspeed < 0) {
                    _xspeed += Math.min( -_xspeed, NATURAL_DECELERATION);
                }
                if (_xspeed == 0) iwata.stand();
            }

            if (_xspeed > MAXIMUM_SPEED) _xspeed = MAXIMUM_SPEED;
            if (_xspeed < -MAXIMUM_SPEED) _xspeed = -MAXIMUM_SPEED;

            iwata.speedRatio = Math.abs(_xspeed / MAXIMUM_SPEED);


            if (FlxG.keys.justPressed.Z) {
                _jumpSound.play();
                iwata.jump();
                iwataState = AIR;
                _yspeed = JUMP_SPEED;

                if (Math.abs(_xspeed) >= FAST_GRAVITY_THRESHOLD) {
                    _gravity = FAST_HOLD_GRAVITY;
                } else {
                    _gravity = HOLD_GRAVITY;
                }
            }
        }
        else if (iwataState == AIR) {
            if (FlxG.keys.pressed.RIGHT && !FlxG.keys.pressed.LEFT) {
                _xspeed += AIR_ACCELERATION;
            } else if (FlxG.keys.pressed.LEFT && !FlxG.keys.pressed.RIGHT) {
                _xspeed -= AIR_ACCELERATION;
            }

            if (_xspeed > MAXIMUM_SPEED) _xspeed = MAXIMUM_SPEED;
            if (_xspeed < -MAXIMUM_SPEED) _xspeed = -MAXIMUM_SPEED;

            iwata.speedRatio = Math.abs(_xspeed / MAXIMUM_SPEED);

            if (!FlxG.keys.pressed.Z) {
                _gravity = REGULAR_GRAVITY;
            }
        }
        else if (iwataState == HIT) {
            if (FlxG.keys.pressed.RIGHT && !FlxG.keys.pressed.LEFT) {
                _xspeed += AIR_ACCELERATION / 2;
            } else if (FlxG.keys.pressed.LEFT && !FlxG.keys.pressed.RIGHT) {
                _xspeed -= AIR_ACCELERATION / 2;
            }

            if (_xspeed > MAXIMUM_SPEED) _xspeed = MAXIMUM_SPEED;
            if (_xspeed < -MAXIMUM_SPEED) _xspeed = -MAXIMUM_SPEED;

            iwata.speedRatio = Math.abs(_xspeed / MAXIMUM_SPEED);
        }
    }

    function updatePosition() {
        _yspeed += _gravity;

        var xMovementResult = currentMap.moveX(_x, _y, _xspeed, hitboxW, hitboxH);
        _x = xMovementResult._x;
        _y = xMovementResult._y;
        _xspeed = xMovementResult._xspeed;

        var yMovementResult = currentMap.moveY(_x, _y, _xspeed, _yspeed, hitboxW, hitboxH);
        _x = yMovementResult._x;
        _y = yMovementResult._y;
        _yspeed = yMovementResult._yspeed;

        if (iwataState == GROUND && !yMovementResult.hasCollision) {
            iwataState = AIR;
            _gravity = DROP_GRAVITY;
            iwata.fall();
        } else if (iwataState == AIR || iwataState == HIT) {
            if (_yspeed >= 0 && _gravity == HOLD_GRAVITY && !yMovementResult.hasCollision) {
                _gravity = REGULAR_GRAVITY;
            } else if (_yspeed >= 0 && _gravity == FAST_HOLD_GRAVITY && !yMovementResult.hasCollision) {
                _gravity = FAST_REGULAR_GRAVITY;
            }

            if (_yspeed >= 0) {
                iwata.fall();
            }

            if (yMovementResult.hasCollision) {
                iwataState = GROUND;
                iwata.run();
            }
        }

        iwata.x = Math.round(_x);
        iwata.y = Math.round(_y);

        cameraFocus.x = iwata.x;
        cameraFocus.y = iwata.y;
    }

    function checkCollision() {
        if (lifeButton != null && FlxG.pixelPerfectOverlap(iwata, lifeButton.hitbox, 1) && _yspeed > 0 && worldState == GREY) {
            worldState = TRANSITIONING;
            fadeLevel = -100;
            fadingIn = true;
            fadeType = "button";
            lifeButton.press();
            FlxG.sound.music.stop();
            _buttonSound.play();
        }

        if (levelItem != null && FlxG.pixelPerfectOverlap(iwata, levelItem.hitbox, 1) && worldState == COLORED) {
            worldState = TRANSITIONING;
            fadeLevel = -100;
            fadingIn = true;
            fadeType = "item";
            _buttonSound.play();
        }
    }

    function updateTransition () {
    }
}
