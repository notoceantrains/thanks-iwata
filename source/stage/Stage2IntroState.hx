package stage;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;

/**
 * ...
 * @author Dahamugo
 */
class Stage2IntroState extends FlxState {
    private var timer: Int = 0;

    public function new() { super(); }

    override public function create():Void {
        super.create();

        var screen = new FlxSprite(0, 0, AssetPaths.Map2Begin__png);
        add(screen);
        timer = 0;
    }

    override public function update(elapsed: Float):Void {
        super.update(elapsed);

        if (timer++ == 120) FlxG.switchState(new Stage2State());
    }

}
