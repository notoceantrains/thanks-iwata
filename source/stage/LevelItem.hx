package stage;

import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;

/**
 * ...
 * @author Dahamugo
 */
class LevelItem extends FlxSpriteGroup {
    private var itemSprite: FlxSprite;
    public var hitbox: FlxSprite;

    public function new(_x:Float=0, _y:Float=0) {
        super(_x, _y);

        this.visible = false;
    }

    public function live() {
        this.visible = true;
    }
}
