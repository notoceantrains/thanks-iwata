package stage;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;

/**
 * ...
 * @author Dahamugo
 */
class Stage1IntroState extends FlxState {
    private var timer: Int = 0;

    override public function create():Void {
        super.create();

        var screen = new FlxSprite(0, 0, AssetPaths.Map1Begin__png);
        add(screen);
        timer = 0;
    }

    override public function update(elapsed: Float):Void {
        super.update(elapsed);

        if (timer++ == 120) FlxG.switchState(new Stage1State());
    }

}
