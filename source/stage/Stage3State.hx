package stage;

import addons.tiled.TiledLayer;
import flixel.FlxCamera;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.FlxG;
import flixel.math.FlxRect;
import flixel.system.FlxSound;
import flixel.math.FlxAngle;
import flixel.util.FlxColor;
import flixel.math.FlxPoint;

/**
 * ...
 * @author Dahamugo
 */
class Stage3State extends StageState {
    private var collectibles: Array<Stage3Collectible> = [];

    private static inline var MINIMUM_SPEED = 19 / 256;
    private static inline var MAXIMUM_SPEED = 25 / 16;
    //private static inline var ACCELERATION = 19 / 512;
    private static inline var ACCELERATION = 37 / 512;
    private static inline var NATURAL_DECELERATION = 13 / 256;
    private static inline var OPPOSITE_DECELERATION = 13 / 128;
    private static inline var SKID_THRESHOLD = 9 / 16;
    //private static inline var AIR_ACCELERATION = 19 / 512;
    private static inline var AIR_ACCELERATION = 26 / 512;
    private static inline var DROP_GRAVITY = 5 / 32;
    private static inline var REGULAR_GRAVITY = 7 / 16;
    private static inline var FAST_REGULAR_GRAVITY = 6 / 16;
    private static inline var HOLD_GRAVITY = 3 / 16;
    private static inline var FAST_HOLD_GRAVITY = 47 / 256;
    private static inline var JUMP_SPEED = -4;
    private static inline var FAST_GRAVITY_THRESHOLD = 1;

    public function new() { super(); }

    override public function create():Void {

        mapLayer = new FlxGroup();
        objectLayer = new FlxGroup();
        buttonLayer = new FlxGroup();
        add(mapLayer);
        add(buttonLayer);
        add(objectLayer);

        _x = 40;
        _y = 80;
        iwata = new Iwata(_x, _y, 2);
        objectLayer.add(iwata);

        cameraFocus = new FlxSprite(20, 80);
        cameraFocus.makeGraphic(1, 1, FlxColor.TRANSPARENT);
        objectLayer.add(cameraFocus);

        FlxG.camera.follow(cameraFocus, FlxCameraFollowStyle.LOCKON);
        FlxG.camera.deadzone = new FlxRect(FlxG.camera.width / 2, FlxG.camera.height / 2, 1, 1);

        currentMap = new GameMap("assets/Map3/Map3.tmx", "Map3TilesOrign", "assets/Map3/");
        for (tilemap in currentMap.tilemapList) {
            tilemap.follow(FlxG.camera);
            mapLayer.add(tilemap);
        }

        currentMap.loadStage3Objects(this);

        fadeCover = new FlxSprite(0, 0);
        fadeCover.makeGraphic(160, 144, FlxColor.WHITE);
        fadeCover.alpha = 0;
        fadeCover.scrollFactor.x = fadeCover.scrollFactor.y = 0;
        add(fadeCover);

        var hud = new FlxSprite(0, 135, AssetPaths.Map2HUD__png);
        hud.scrollFactor.x = hud.scrollFactor.y = 0;
        add(hud);

        endFadeCover = new FlxSprite(0, 0);
        endFadeCover.makeGraphic(160, 144, FlxColor.WHITE);
        endFadeCover.alpha = 0;
        endFadeCover.scrollFactor.x = endFadeCover.scrollFactor.y = 0;
        add(endFadeCover);

        _jumpSound = FlxG.sound.load(AssetPaths.Jump__wav);
        _obstacleSound = FlxG.sound.load(AssetPaths.Obstacle__wav);
        _collectibleSound = FlxG.sound.load(AssetPaths.Map2CollectGrab__wav);
        _buttonSound = FlxG.sound.load(AssetPaths.LifebuttonPress__wav);
        _buttonFadeSound = FlxG.sound.load(AssetPaths.LifebuttonFade2__wav);
        FlxG.sound.playMusic(AssetPaths.BGM1__wav, 0.75, true);
    }

    override function checkCollision() {
        if (--hitstun >= 0) return;

        if (worldState == COLORED) {
            for (collectible in collectibles) {
                if (Math.abs(_x - collectible.x) > 64 || Math.abs(_y - collectible.y) > 64) {}
                else if (FlxG.pixelPerfectOverlap(iwata, collectible)) {
                    if (collectible.collect()) {
                        _collectibleSound.play(true);
                    }
                }
            }
        }

        super.checkCollision();
    }

    public function addCollectible(tileX: Int, tileY: Int, type: Int) {
        var collectible = new Stage3Collectible(tileX, tileY, type);
        objectLayer.add(collectible);
        collectibles.push(collectible);
    }

    public function addButton(tileX: Int, tileY: Int) {
        lifeButton = new LifeButton(tileX, tileY, true);
        buttonLayer.add(lifeButton);
    }

    public function addLevelItem(tileX: Int, tileY: Int) {
        levelItem = new Stage2LevelItem(tileX, tileY);
        objectLayer.add(levelItem);
    }

    override function updateTransition () {
        if (fadeType == "button") {
            if (fadingIn) {
                fadeCover.visible = true;
                fadeLevel = fadeLevel + 2 > 150 ? 150 : fadeLevel + 2;
                fadeCover.alpha = (Math.ceil(fadeLevel / 33) * 33 + 1) / 100;
                if (fadeLevel == 20) _buttonFadeSound.play();
                if (fadeLevel == 150) {
                    fadingIn = false;
                    for (collectible in collectibles) {
                        collectible.live();
                    }
                    lifeButton.live();
                    if (levelItem != null) {
                        levelItem.live();
                    }
                    for (tilemap in currentMap.tilemapList) {
                        mapLayer.remove(tilemap);
                    }
                    currentMap.live("Map3TilesColor");
                    for (tilemap in currentMap.tilemapList) {
                        mapLayer.add(tilemap);
                    }
                }
            } else {
                fadeLevel = fadeLevel - 2 < 0 ? 0 : fadeLevel - 2;
                fadeCover.alpha = (Math.floor(fadeLevel / 33) * 33 + 1) / 100;
                if (fadeLevel == 0) {
                    worldState = COLORED;
                    FlxG.sound.playMusic(AssetPaths.BGM2__wav, 0.75, true);
                }
            }
        }
        else if (fadeType == "item") {
            if (fadingIn) {
                endFadeCover.visible = true;
                fadeLevel = fadeLevel + 2 > 150 ? 150 : fadeLevel + 2;
                endFadeCover.alpha = (Math.ceil(fadeLevel / 33) * 33 + 1) / 100;
                if (fadeLevel == 20) _buttonFadeSound.play();
                if (fadeLevel == 150) {
                    FlxG.switchState(new Stage2EndState());
                }
            }
        }
    }
}
