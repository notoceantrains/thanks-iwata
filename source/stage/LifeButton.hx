package stage;

import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.util.FlxColor;

/**
 * ...
 * @author Dahamugo
 */
class LifeButton extends FlxSpriteGroup {
    private var buttonSprite: FlxSprite;
    public var hitbox: FlxSprite;

    public function new(_x:Float=0, _y:Float=0, level2: Bool = false) {
        super(_x, _y);
        
        buttonSprite = new FlxSprite();
		buttonSprite.loadGraphic(AssetPaths.Lifebutton__png, true, 32, 16);
        if (level2) {
            buttonSprite.animation.add("original", [0], 1, true);
            buttonSprite.animation.add("pressed", [1], 1, true);
            buttonSprite.animation.add("color", [2, 3, 4], 6, true);

        } else {
            buttonSprite.animation.add("original", [5], 1, true);
            buttonSprite.animation.add("pressed", [6], 1, true);
            buttonSprite.animation.add("color", [7, 8, 9], 6, true);
        }
        buttonSprite.animation.play("original");
        
        add(buttonSprite);
        
        hitbox = new FlxSprite(10, 8);
        hitbox.makeGraphic(12, 8, 0x01FFFFFF);
        add(hitbox);
    }
    
    public function live() {
        buttonSprite.animation.play("color");
    }
    
    public function press() {
        buttonSprite.animation.play("pressed");
    }
}