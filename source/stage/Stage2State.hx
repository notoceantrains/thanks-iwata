package stage;

import addons.tiled.TiledLayer;
import flixel.FlxCamera;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.FlxG;
import flixel.math.FlxRect;
import flixel.system.FlxSound;
import flixel.math.FlxAngle;
import flixel.util.FlxColor;
import flixel.math.FlxPoint;

/**
 * ...
 * @author Dahamugo
 */
class Stage2State extends StageState {
    private var collectibles: Array<Stage2Collectible> = [];
    private var enemies: Array<Stage2Enemy> = [];
    private var jumpingEnemies: Array<Stage2JumpingEnemy> = [];

    private static inline var MINIMUM_SPEED = 19 / 256;
    private static inline var MAXIMUM_SPEED = 25 / 16;
    //private static inline var ACCELERATION = 19 / 512;
    private static inline var ACCELERATION = 37 / 512;
    private static inline var NATURAL_DECELERATION = 13 / 256;
    private static inline var OPPOSITE_DECELERATION = 13 / 128;
    private static inline var SKID_THRESHOLD = 9 / 16;
    //private static inline var AIR_ACCELERATION = 19 / 512;
    private static inline var AIR_ACCELERATION = 26 / 512;
    private static inline var DROP_GRAVITY = 5 / 32;
    private static inline var REGULAR_GRAVITY = 7 / 16;
    private static inline var FAST_REGULAR_GRAVITY = 6 / 16;
    private static inline var HOLD_GRAVITY = 3 / 16;
    private static inline var FAST_HOLD_GRAVITY = 47 / 256;
    private static inline var JUMP_SPEED = -4;
    private static inline var FAST_GRAVITY_THRESHOLD = 1;

    private var rightCar: Stage2Car;
    private var rightCarCountdown: Int = 0;
    private var leftCar: Stage2Car;
    private var leftCarCountdown: Int = 0;
    private var rightCarLayer: FlxGroup;
    private var leftCarLayer: FlxGroup;
    private var signLayer: FlxGroup;

    private var signs: Array<Stage2Sign> = [];

    public function new() { super(); }

    override public function create():Void {

        mapLayer = new FlxGroup();
        objectLayer = new FlxGroup();
        buttonLayer = new FlxGroup();
        rightCarLayer = new FlxGroup();
        leftCarLayer = new FlxGroup();
        signLayer = new FlxGroup();
        add(mapLayer);
        add(buttonLayer);
        add(objectLayer);
        add(leftCarLayer);
        add(rightCarLayer);
        add(signLayer);

        _x = 20;
        _y = 80;
        iwata = new Iwata(_x, _y, 2);
        objectLayer.add(iwata);

        cameraFocus = new FlxSprite(20, 80);
        cameraFocus.makeGraphic(1, 1, FlxColor.TRANSPARENT);
        objectLayer.add(cameraFocus);

        FlxG.camera.follow(cameraFocus, FlxCameraFollowStyle.LOCKON);
        FlxG.camera.deadzone = new FlxRect(FlxG.camera.width / 2, FlxG.camera.height / 2, 1, 1);

        currentMap = new GameMap("assets/Map2/Map2.tmx", "Map2TilesOrign", "assets/Map2/");
        for (tilemap in currentMap.tilemapList) {
            tilemap.follow(FlxG.camera);
            mapLayer.add(tilemap);
        }

        currentMap.loadStage2Objects(this);

        fadeCover = new FlxSprite(0, 0);
        fadeCover.makeGraphic(160, 144, FlxColor.WHITE);
        fadeCover.alpha = 0;
        fadeCover.scrollFactor.x = fadeCover.scrollFactor.y = 0;
        add(fadeCover);

        var hud = new FlxSprite(0, 135, AssetPaths.Map2HUD__png);
        hud.scrollFactor.x = hud.scrollFactor.y = 0;
        add(hud);

        endFadeCover = new FlxSprite(0, 0);
        endFadeCover.makeGraphic(160, 144, FlxColor.WHITE);
        endFadeCover.alpha = 0;
        endFadeCover.scrollFactor.x = endFadeCover.scrollFactor.y = 0;
        add(endFadeCover);

        _jumpSound = FlxG.sound.load(AssetPaths.Jump__wav);
        _obstacleSound = FlxG.sound.load(AssetPaths.Obstacle__wav);
        _collectibleSound = FlxG.sound.load(AssetPaths.Map2CollectGrab__wav);
        _buttonSound = FlxG.sound.load(AssetPaths.LifebuttonPress__wav);
        _buttonFadeSound = FlxG.sound.load(AssetPaths.LifebuttonFade2__wav);
        FlxG.sound.playMusic(AssetPaths.BGM1__wav, 0.75, true);
    }

    override function checkCollision() {
        if (--hitstun >= 0) return;

        if (worldState == COLORED) {
            for (collectible in collectibles) {
                if (Math.abs(_x - collectible.x) > 64 || Math.abs(_y - collectible.y) > 64) {}
                else if (FlxG.pixelPerfectOverlap(iwata, collectible)) {
                    if (collectible.collect()) {
                        _collectibleSound.play(true);
                    }
                }
            }
        }

        for (enemy in enemies) {
            if (Math.abs(_x - enemy.x) > 64 || Math.abs(_y - enemy.y) > 64) {}
            else if (FlxG.pixelPerfectOverlap(iwata, enemy)) {
                var angle = (new FlxPoint(_x, _y).angleBetween(new FlxPoint(enemy.x + 8, enemy.y + 32)) + 90) * FlxAngle.TO_RAD;

                var minXSpeed: Float = 0;
                var minYSpeed: Float = 0;
                if (Math.cos(angle) < 0) {
                    iwata.faceRight();
                    minXSpeed = -3/5;
                } else if (Math.cos(angle) > 0) {
                    iwata.faceLeft();
                    minXSpeed = 3/5;
                }

                if (_y > (enemy.y + 32)) {
                    minYSpeed = 3;
                } else {
                    minYSpeed = -3;
                }

                _xspeed = (Math.abs(_xspeed) * 1/2 + 1/2) * Math.cos(angle) + minXSpeed;
                _yspeed = (Math.abs(_yspeed) * 1 / 2) * Math.sin(angle) +minYSpeed;

                //enemy.live();

                iwata.hit();
                iwataState = HIT;
                hitstun = 5;
                _gravity = REGULAR_GRAVITY;
                _obstacleSound.play(true);
            }
        }

        for (enemy in jumpingEnemies) {
            if (Math.abs(_x - enemy.x) > 64 || Math.abs(_y - enemy.y) > 64) {}
            else if (FlxG.pixelPerfectOverlap(iwata, enemy)) {
                var angle = (new FlxPoint(_x, _y).angleBetween(new FlxPoint(enemy.x + 8, enemy.y + 32)) + 90) * FlxAngle.TO_RAD;

                var minXSpeed: Float = 0;
                var minYSpeed: Float = 0;
                if (Math.cos(angle) < 0) {
                    iwata.faceRight();
                    minXSpeed = -3/5;
                } else if (Math.cos(angle) > 0) {
                    iwata.faceLeft();
                    minXSpeed = 3/5;
                }

                if (_y > (enemy.y + 32)) {
                    minYSpeed = 3;
                } else {
                    minYSpeed = -3;
                }

                _xspeed = (Math.abs(_xspeed) * 1/2 + 1/2) * Math.cos(angle) + minXSpeed;
                _yspeed = (Math.abs(_yspeed) * 1 / 2) * Math.sin(angle) + minYSpeed + enemy.ySpeed / 2;

                //enemy.live();

                iwata.hit();
                iwataState = HIT;
                hitstun = 5;
                _gravity = REGULAR_GRAVITY;
                _obstacleSound.play(true);
            } else if (Math.abs(_x - (enemy.x + 8)) < 24 && enemy.state == COLORED && _yspeed < 0) {
                enemy.jump();
            }
        }

        super.checkCollision();
    }

    public function addCollectible(tileX: Int, tileY: Int, type: Int) {
        var collectible = new Stage2Collectible(tileX, tileY, type);
        objectLayer.add(collectible);
        collectibles.push(collectible);
    }

    public function addButton(tileX: Int, tileY: Int) {
        lifeButton = new LifeButton(tileX, tileY, true);
        buttonLayer.add(lifeButton);
    }

    public function addLevelItem(tileX: Int, tileY: Int) {
        levelItem = new Stage2LevelItem(tileX, tileY);
        objectLayer.add(levelItem);
    }

    public function addSign(tileX: Int, tileY: Int) {
        var sign = new Stage2Sign(tileX, tileY);
        signs.push(sign);
        signLayer.add(sign);
    }

    public function addEnemy(tileX: Int, tileY: Int, initial: String, leftrange: Int, rightrange: Int, sprite: Int) {
        var enemy = new Stage2Enemy(tileX, tileY, initial, leftrange, rightrange, sprite);
        objectLayer.add(enemy);
        enemies.push(enemy);
    }

    public function addJumpingEnemy(tileX: Int, tileY: Int, sprite: Int) {
        var enemy = new Stage2JumpingEnemy(tileX, tileY, sprite);
        objectLayer.add(enemy);
        jumpingEnemies.push(enemy);
    }

    override function updateTransition () {
        if (fadeType == "button") {
            if (fadingIn) {
                fadeCover.visible = true;
                fadeLevel = fadeLevel + 2 > 150 ? 150 : fadeLevel + 2;
                fadeCover.alpha = (Math.ceil(fadeLevel / 33) * 33 + 1) / 100;
                if (fadeLevel == 20) _buttonFadeSound.play();
                if (fadeLevel == 150) {
                    fadingIn = false;
                    for (collectible in collectibles) {
                        collectible.live();
                    }
                    for (sign in signs) {
                        sign.live();
                    }
                    for (enemy in enemies) {
                        enemy.live();
                    }
                    for (enemy in jumpingEnemies) {
                        enemy.live();
                    }
                    lifeButton.live();
                    if (levelItem != null) {
                        levelItem.live();
                    }
                    for (tilemap in currentMap.tilemapList) {
                        mapLayer.remove(tilemap);
                    }
                    currentMap.live("Map2TilesColor");
                    for (tilemap in currentMap.tilemapList) {
                        mapLayer.add(tilemap);
                    }
                }
            } else {
                fadeLevel = fadeLevel - 2 < 0 ? 0 : fadeLevel - 2;
                fadeCover.alpha = (Math.floor(fadeLevel / 33) * 33 + 1) / 100;
                if (fadeLevel == 0) {
                    worldState = COLORED;
                    startRightCarCountdown();
                    startLeftCarCountdown();
                    FlxG.sound.playMusic(AssetPaths.BGM2__wav, 0.75, true);
                }
            }
        }
        else if (fadeType == "item") {
            if (fadingIn) {
                endFadeCover.visible = true;
                fadeLevel = fadeLevel + 2 > 150 ? 150 : fadeLevel + 2;
                endFadeCover.alpha = (Math.ceil(fadeLevel / 33) * 33 + 1) / 100;
                if (fadeLevel == 20) _buttonFadeSound.play();
                if (fadeLevel == 150) {
                    FlxG.switchState(new Stage2EndState());
                }
            }
        }
    }

    override public function update(elapsed: Float):Void {
        super.update(elapsed);

        if (rightCarCountdown > 0) {
            rightCarCountdown--;
            if (rightCarCountdown == 0) {
                spawnRightCar();
            }
        }

        if (leftCarCountdown > 0) {
            leftCarCountdown --;
            if (leftCarCountdown  == 0) {
                spawnLeftCar();
            }
        }

        if (rightCar != null) {
            rightCar.x = rightCar.x + Math.ceil(rightCar.speed);
            if (rightCar.x > _x + 80 + 48 + 20) {
                rightCarLayer.remove(rightCar);
                rightCar = null;
                startRightCarCountdown();
            }
        }

        if (leftCar != null) {
            leftCar.x = leftCar.x - Math.ceil(leftCar.speed);
            if (leftCar.x < _x - 80 - 48 - 20) {
                leftCarLayer.remove(leftCar);
                leftCar = null;
                startLeftCarCountdown();
            }
        }
    }

    private function startRightCarCountdown() {
        rightCarCountdown = 60 + Math.floor(Math.random() * 180);
    }

    private function startLeftCarCountdown() {
        leftCarCountdown = 120 + Math.floor(Math.random() * 180);
    }

    private function spawnRightCar() {
        rightCar = new Stage2Car(_x - 80 - 48, 95, false);
        rightCarLayer.add(rightCar);
    }

    private function spawnLeftCar() {
        leftCar = new Stage2Car(_x + 80 + 48, 83, true);
        leftCarLayer.add(leftCar);
    }
}
