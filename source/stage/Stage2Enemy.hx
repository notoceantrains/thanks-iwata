package stage;

import flixel.FlxSprite;

/**
 * ...
 * @author Dahamugo
 */

enum Enemy2Facing {
    LEFT;
    RIGHT;
    DOWN;
}

enum Enemy2State {
    ORIGINAL;
    COLORED;
}

class Stage2Enemy extends FlxSprite {
    private static inline var ENEMY_SPEED = 5/8;

    private var face: Enemy2Facing;
    private var state: Enemy2State = ORIGINAL;
    private var leftmost: Float;
    private var rightmost: Float;
    private var downCountdown: Int;
    private var nextDirection: String;

    public function new(_x:Float=0, _y:Float=0, initial: String, leftrange: Int, rightrange: Int, sprite: Int) {
        super(_x, _y);
        nextDirection = initial;

        leftmost = _x - leftrange * 16;
        rightmost = _x + rightrange * 16;

        if (sprite == 1) {
            this.loadGraphic(AssetPaths.Map2Obstacle1__png, true, 16, 32);
        } else {
            this.loadGraphic(AssetPaths.Map2Obstacle2__png, true, 16, 32);
        }

        this.animation.add("original", [0], 1, true);
        this.animation.add("walkdown", [1, 2], 5, true);
        this.animation.add("walkleft", [3, 4], 5, true);
        this.animation.add("walkright", [5, 6], 5, true);
        this.animation.play("original");
    }

    public function live() {
        if (state == COLORED)
            return;

        if (nextDirection == "left") {
            walkLeft();
        } else {
            walkRight();
        }
        state = COLORED;
    }

    public function walkLeft() {
        this.animation.play("walkleft");
        face = LEFT;
    }

    public function walkRight() {
        this.animation.play("walkright");
        face = RIGHT;
    }

    public function walkDown() {
        this.animation.play("walkdown");
        downCountdown = 60;
        face = DOWN;
    }

    override public function update(elapsed: Float):Void {
        super.update(elapsed);

        if (state == COLORED) {
            switch(face) {
                case LEFT:
                    x -= ENEMY_SPEED;
                    if (x <= leftmost) {
                        nextDirection = "right";
                        walkDown();
                    }

                case RIGHT:
                    x += ENEMY_SPEED;
                    if (x >= rightmost) {
                        nextDirection = "left";
                        walkDown();
                    }

                case DOWN: {
                    downCountdown--;
                    if (downCountdown <= 0) {
                        if (nextDirection == "left") {
                            walkLeft();
                        } else {
                            walkRight();
                        }
                    }
                }
            }
        }
    }
}
