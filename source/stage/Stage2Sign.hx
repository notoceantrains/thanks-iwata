package stage;
import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class Stage2Sign extends FlxSprite {
    private static inline var MINIMUM_SPEED = 25 / 16;
    private static inline var MAXIMUM_SPEED = 60 / 16;

    public var speed: Float;

    public function new(_x:Float = 0, _y:Float = 0) {
        super(_x, _y);

        this.loadGraphic(AssetPaths.Map2Sign__png, true, 16, 32);
        this.animation.add("original", [0], 1, true);
        this.animation.add("color", [1], 1, true);
        this.animation.play("original");
    }

    public function live() {
        this.animation.play("color");
    }
}
