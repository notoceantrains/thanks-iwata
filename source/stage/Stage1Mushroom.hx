package stage;

import flixel.FlxSprite;

/**
 * ...
 * @author Dahamugo
 */
class Stage1Mushroom extends FlxSprite{

    public function new(_x:Float=0, _y:Float=0) {
        super(_x, _y);
		this.loadGraphic(AssetPaths.Map1Obstacle1__png, true, 16, 16);
        this.animation.add("original", [0], 1, true);
        this.animation.add("color", [1, 2, 3, 4], 2, true);
        this.animation.play("original");
    }

    public function live() {
        this.animation.play("color");
    }
}
