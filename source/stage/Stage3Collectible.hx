package stage;

import flixel.FlxSprite;

/**
 * ...
 * @author Dahamugo
 */
class Stage3Collectible extends FlxSprite {
    private var isLive: Bool = false;
    private var wasCollected: Bool = false;

    public function new(_x:Float=0, _y:Float=0, type: Int) {
        super(_x, _y);
        if (type == 1) {
    		this.loadGraphic(AssetPaths.Map2CollectA__png, true, 16, 16);
        } else {
            this.loadGraphic(AssetPaths.Map2CollectB__png, true, 16, 16);
        }
        this.animation.add("original", [0], 1, true);
        this.animation.add("color", [0, 1, 2, 3], 10, true);
        this.animation.add("collect", [4, 5, 6], 5, false);
        this.animation.play("original");
        this.visible = false;
    }

    public function live() {
        this.animation.play("color");
        this.visible = true;
        this.isLive = true;
    }

    public function collect() {
        this.animation.play("collect");

        var r = !wasCollected;
        wasCollected = true;
        return r;
    }
}
