package stage;

import flixel.FlxSprite;

/**
 * ...
 * @author Dahamugo
 */
class Stage1Collectible extends FlxSprite {
    private var isLive: Bool = false;
    private var wasCollected: Bool = false;

    public function new(_x:Float=0, _y:Float=0) {
        super(_x, _y);
		this.loadGraphic(AssetPaths.Map1Collect__png, true, 16, 16);
        this.animation.add("original", [0], 1, true);
        this.animation.add("color", [1, 2, 3, 4], 10, true);
        this.animation.add("collect", [5, 6, 7, 8], 10, false);
        this.animation.play("original");
    }
    
    public function live() {
        this.animation.play("color");
        isLive = true;
    }
    
    public function collect() {
        if (isLive) {
            this.animation.play("collect");
        } else {
            this.visible = false;
        }
        
        var r = !wasCollected;
        wasCollected = true;
        return r;
    }
}