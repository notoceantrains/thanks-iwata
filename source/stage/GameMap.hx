package stage;

import addons.tiled.TiledLayer;
import addons.tiled.TiledPropertySet;
import openfl.Assets;
import haxe.io.Path;
import haxe.xml.Parser;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.tile.FlxTilemap;
import addons.tiled.TiledMap;
import addons.tiled.TiledObject;
import addons.tiled.TiledTileSet;
import addons.tiled.TiledObjectGroup;

class GameMap extends TiledMap
{
	// For each "Tile Layer" in the map, you must define a "tileset" property which contains the name of a tile sheet image
	// used to draw tiles in that layer (without file extension). The image file must be located in the directory specified bellow.

    public var layoutType: String;
    public var baseLayer: TiledLayer;
    public var tilemapList: Array<FlxTilemap>;
    private var _assetsPath: String;

	public function new(map:Dynamic, tileSheetName: String, assetsPath: String)
	{
		super(map);

        _assetsPath = assetsPath;
        tilemapList = new Array<FlxTilemap>();

		// Load Tile Maps
		for (tileLayer in layers)
		{
			var tileSet:TiledTileSet = null;
			for (ts in tilesets)
			{
				if (ts.firstGID == 1)
				{
					tileSet = ts;
					break;
				}
			}

			var processedPath 	= assetsPath + tileSheetName + ".png";

            var tilemap = new FlxTilemap();
            tilemap.setSize(width, height);
            tilemap.loadMapFromArray(tileLayer.tileArray, width, height, processedPath, 16, 16, null, 1);
            tilemapList.push(tilemap);

            if (tileLayer.name == "Foreground" || baseLayer == null) {
                baseLayer = tileLayer;
            }
		}
	}

    private function getTileIndex (tileX: Int, tileY: Int): Int {
        return tileX + tileY * this.width;
    }

    public function getTileProperties(tileX: Int, tileY: Int, ?layer:TiledLayer): TiledPropertySet {
        var properties: TiledPropertySet = null;

        if (layer == null) layer = baseLayer;

        var tileIndex = getTileIndex(tileX, tileY);

        var tileId = layer.tileArray[tileIndex];

        for (tileset in this.tilesets)
		{
			if (tileset.hasGid(tileId))
			{
                properties = tileset.getPropertiesByGid(tileId);
                break;
			}
		}

        return properties;
    }

    public function hasGround (_x: Float, _y: Float) {
        var _lefttileX = Math.floor((_x - 6) / 16);
        var _righttileX = Math.floor((_x + 6) / 16);
        var _tileY = Math.floor(_y / 16);

        var _lefttileProperties = getTileProperties(_lefttileX, _tileY);
        var _righttileProperties = getTileProperties(_righttileX, _tileY);

        var has = false;

        if (_lefttileProperties != null && _lefttileProperties.contains("solid")) {
            if (_lefttileProperties.get("solid") == "1") {
                has = true;
            } else if (_lefttileProperties.get("solid") == "2") {
                if (Math.abs(_y - 16 * _tileY) <= 6) has = true;
            }
        }
        if (_righttileProperties != null && _righttileProperties.contains("solid")){
            if (_righttileProperties.get("solid") == "1") {
                has = true;
            } else if (_righttileProperties.get("solid") == "2") {
                if (Math.abs(_y - 16 * _tileY) <= 6) has = true;
            }
        }

        return has;
    }

   public function moveX(_x: Float, _y: Float, _xspeed: Float, hitboxW: Int, hitboxH: Int): {_x: Float, _y: Float, _xspeed: Float, hasCollision: Bool} {
        if (_xspeed == 0) return { _x: _x, _y: _y, _xspeed: _xspeed, hasCollision: false};

        var edgeX: Float;
        var edgeTileX: Int;
        var scanX: Int;

        if (_xspeed > 0) {
            edgeX = _x + hitboxW / 2;
            scanX = tilemapList[0].widthInTiles;
        } else {
            edgeX = _x - hitboxW / 2;
            scanX = 0;
        }

        var bottomEdgeY = _y - 1;
        var topEdgeY = _y - hitboxH;

        edgeTileX = Math.floor(edgeX / 16);
        var bottomEdgeTileY = Math.floor(bottomEdgeY / 16);
        var topEdgeTileY = Math.floor(topEdgeY / 16);

        var shortestDistance = _xspeed;
        var hasCollision = false;

        var playerTileX = Math.floor(_x / 16);
        var playerTileY = bottomEdgeTileY;
        var ignoreRow = false;

        for (tileY in topEdgeTileY...(bottomEdgeTileY + 1)) {
            if (tileY == playerTileY && ignoreRow) continue;

            var tileX = edgeTileX;

            if (_xspeed > 0) {
                while (tileX <= scanX) {
                    var tileEdge = tileX * 16;
                    if ((tileEdge - edgeX) > shortestDistance) break;
                    var tileProperties = getTileProperties(tileX, tileY);
                    if (tileProperties != null && tileProperties.contains("solid") && tileProperties.get("solid") == "1") {
                        shortestDistance = tileEdge - edgeX;
                        hasCollision = true;
                        break;
                    }
                    tileX++;
                }
            } else {
                while (tileY >= scanX) {
                    var tileEdge = tileX * 16 + 15;
                    if ((tileEdge - edgeX) < shortestDistance) break;
                    var tileProperties = getTileProperties(tileX, tileY);
                    if (tileProperties != null && tileProperties.contains("solid") && tileProperties.get("solid") == "1") {
                        shortestDistance = tileEdge - edgeX;
                        hasCollision = true;
                        break;
                    }
                    tileX--;
                }
            }
        }

        _x = _x + shortestDistance;
        _xspeed = hasCollision ? 0 : _xspeed;

        var playerTileX = Math.floor(_x / 16);

        return {_x: _x, _y: _y, _xspeed: _xspeed, hasCollision: hasCollision};
    }

    public function moveY(_x: Float, _y: Float, _xspeed: Float, _yspeed: Float, hitboxW: Int, hitboxH: Int): { _x: Float, _y: Float, _yspeed: Float, hasCollision: Bool } {
        if (_yspeed == 0) return { _x: _x, _y: _y, _yspeed: _yspeed, hasCollision: false };

        var edgeY: Float;
        var edgeTileY: Int;
        var scanY: Int;

        if (_yspeed > 0) {
            edgeY = _y;
            scanY = tilemapList[0].heightInTiles;
        } else {
            edgeY = _y - hitboxH - 1;
            scanY = 0;
        }
        var leftEdgeX = _x - (hitboxW / 2 - 1);
        var rightEdgeX = _x + (hitboxW / 2 - 1);

        edgeTileY = Math.floor(edgeY / 16);
        var leftEdgeTileX = Math.floor(leftEdgeX / 16);
        var rightEdgeTileX = Math.floor(rightEdgeX / 16);

        var shortestDistance = _yspeed;
        var hasCollision = false;

        var playerTileX = Math.floor(_x / 16);
        var playerTileY = Math.floor((_y - 1) / 16);

        if (_yspeed > 0) {
            var tileY = edgeTileY;
            while (tileY <= scanY) {
                var tileEdge = tileY * 16;
                if ((tileEdge - edgeY) > shortestDistance) break;
                for (tileX in leftEdgeTileX...(rightEdgeTileX + 1)) {
                    var tileProperties = getTileProperties(tileX, tileY);
                    if (tileProperties != null && tileProperties.contains("solid") && tileProperties.get("solid") == "1") {
                        shortestDistance = tileEdge - edgeY;
                        hasCollision = true;
                        break;
                    } else if (tileProperties != null && tileProperties.contains("solid") && tileProperties.get("solid") == "2" && edgeY <= tileEdge) {
                        shortestDistance = tileEdge - edgeY;
                        hasCollision = true;
                        break;
                    }
                }
                tileY++;
            }
        } else {
            var tileY = edgeTileY;
            while (tileY >= scanY) {
                var tileEdge = tileY * 16 + 15;
                if ((tileEdge - edgeY) < shortestDistance) break;
                for (tileX in leftEdgeTileX...(rightEdgeTileX + 1)) {
                    var tileProperties = getTileProperties(tileX, tileY);
                    if (tileProperties != null && tileProperties.contains("solid") && tileProperties.get("solid") == "1") {
                        shortestDistance = tileEdge - edgeY;
                        hasCollision = true;
                        break;
                    }
                }
                tileY--;
            }
        }

        _y = _y + shortestDistance;
        _yspeed = hasCollision ? 0 : _yspeed;
        return {_x: _x, _y: _y, _yspeed: _yspeed, hasCollision: hasCollision};
    }

    private function isSolid(tileX, tileY): Bool {
        var tileProperties = getTileProperties(tileX, tileY);
        return (tileProperties != null && tileProperties.contains("solid") && tileProperties.get("solid") == "1");
    }

    private function isTypeSolid(tileX, tileY): Bool {
        var tileProperties = getTileProperties(tileX, tileY);
        return (tileProperties != null && tileProperties.contains("solid"));
    }



    /*-------------------*/

    public function loadStage1Objects(state:Stage1State)
	{
		for (group in objectGroups)
		{
			for (o in group.objects)
			{
				loadStage1Object(o, group, state);
			}
		}
	}

	private function loadStage1Object(o:TiledObject, g:TiledObjectGroup, state:Stage1State)
	{
		var x:Int = o.x;
		var y:Int = o.y;

		// objects in tiled are aligned bottom-left (top-left in flixel)
		if (o.gid != -1)
			o.y -= g.map.getGidOwner(o.gid).tileHeight;

        var tileX = Math.floor(o.x / 16);
        var tileY = Math.floor(o.y / 16);

		switch (o.type.toLowerCase())
		{
            case "mushroom":
                state.addMushroom(o.x, o.y);

            case "enemy":
                var orientation = o.custom.get("initial");
                var leftrange = Std.parseInt(o.custom.get("leftrange"));
                var rightrange = Std.parseInt(o.custom.get("rightrange"));
                state.addEnemy(o.x, o.y, orientation, leftrange, rightrange);

            case "collectible":
                state.addCollectible(o.x, o.y);

            case "button":
                state.addButton(o.x, o.y);

            case "cloud1":
                state.addCloud(o.x, o.y, 1);

            case "cloud2":
                state.addCloud(o.x, o.y, 2);

            case "cloud3":
                state.addCloud(o.x, o.y, 3);

            case "cloud4":
                state.addCloud(o.x, o.y, 4);

            case "item1a":
                state.addLevelItem(o.x, o.y);

		}
	}

    public function loadStage2Objects(state:Stage2State)
	{
		for (group in objectGroups)
		{
			for (o in group.objects)
			{
				loadStage2Object(o, group, state);
			}
		}
	}

	private function loadStage2Object(o:TiledObject, g:TiledObjectGroup, state:Stage2State)
	{
		var x:Int = o.x;
		var y:Int = o.y;

		// objects in tiled are aligned bottom-left (top-left in flixel)
		if (o.gid != -1)
			o.y -= g.map.getGidOwner(o.gid).tileHeight;

        var tileX = Math.floor(o.x / 16);
        var tileY = Math.floor(o.y / 16);

		switch (o.type.toLowerCase())
		{
            case "collectible1":
                state.addCollectible(o.x, o.y, 1);

            case "collectible2":
                state.addCollectible(o.x, o.y, 2);

            case "button":
                state.addButton(o.x, o.y);

            case "item2a":
                state.addLevelItem(o.x, o.y);

            case "signa":
                state.addSign(o.x, o.y);

            case "enemy":
                var orientation = o.custom.get("initial");
                var leftrange = Std.parseInt(o.custom.get("leftrange"));
                var rightrange = Std.parseInt(o.custom.get("rightrange"));
                var sprite = Std.parseInt(o.custom.get("sprite"));
                state.addEnemy(o.x, o.y - 16, orientation, leftrange, rightrange, sprite);

            case "jumping":
                var sprite = Std.parseInt(o.custom.get("sprite"));
                state.addJumpingEnemy(o.x, o.y - 16, sprite);
		}
	}

    public function loadStage3Objects(state:Stage3State)
	{
		for (group in objectGroups)
		{
			for (o in group.objects)
			{
				loadStage3Object(o, group, state);
			}
		}
	}

    private function loadStage3Object(o:TiledObject, g:TiledObjectGroup, state:Stage3State)
	{
		var x:Int = o.x;
		var y:Int = o.y;

		// objects in tiled are aligned bottom-left (top-left in flixel)
		if (o.gid != -1)
			o.y -= g.map.getGidOwner(o.gid).tileHeight;

        var tileX = Math.floor(o.x / 16);
        var tileY = Math.floor(o.y / 16);

		switch (o.type.toLowerCase())
		{
            case "button":
                state.addButton(o.x, o.y);
		}
	}

    public function live(tileSheetName: String)
	{
        var i = 0;
		// Load Tile Maps
		for (tileLayer in layers)
		{
            var oldData = tilemapList[i].getData();
			var tileSet:TiledTileSet = null;
			for (ts in tilesets)
			{
				if (ts.firstGID == 1)
				{
					tileSet = ts;
					break;
				}
			}

			var processedPath = _assetsPath + tileSheetName + ".png";

			var tilemap = new FlxTilemap();
            tilemap.setSize(width, height);
			tilemap.loadMapFromArray(oldData, width, height, processedPath, 16, 16, null, 1);

            tilemapList[i] = tilemap;

            i++;
		}
	}
}
