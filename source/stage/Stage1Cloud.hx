package stage;

import flixel.FlxSprite;

/**
 * ...
 * @author Dahamugo
 */
class Stage1Cloud extends FlxSprite {

    public function new(_x:Float=0, _y:Float=0, type: Int) {
        super(_x, _y);
        switch (type) {
            case 1:
          		this.loadGraphic(AssetPaths.Map1BG1__png, true, 16, 16);

            case 2:
          		this.loadGraphic(AssetPaths.Map1BG2__png, true, 16, 16);

            case 3:
          		this.loadGraphic(AssetPaths.Map1BG3__png, true, 16, 16);

            case 4:
          		this.loadGraphic(AssetPaths.Map1BG4__png, true, 16, 16);
        }
        this.animation.add("original", [0], 1, true);
        this.animation.add("color", [1, 2], 2, true);
        this.animation.play("original");
    }

    public function live() {
        this.animation.play("color");
    }
}
