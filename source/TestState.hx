package;

import flixel.FlxState;

/**
 * ...
 * @author Dahamugo
 */
class TestState extends FlxState {
    
    private var _numberOfRolls: Int = 0;
    private var _totalCount: Int = 0;

    
    override public function create():Void {
        super.create();
        
        for (i in 3...5) {
            for (n in 3...5) {
                traceResult(i, n);
            }
        }
    }
    
    function getRoll(): Int {
        return Math.floor(Math.random() * 6) + 1;
    }
    
    function simulateSequence(thresholdForSecond: Int = 0, thresholdForThird: Int = 0) {
        var roll = getRoll();
        
        if (roll <= thresholdForSecond) {
            roll = getRoll();
            
            if (roll <= thresholdForThird) roll = getRoll();
        }
        
        return roll;
    }
    
    function simulateABunchOfRolls (thresholdForSecond: Int = 0, thresholdForThird: Int = 0): Float {
        var _numberOfRolls: Int = 0;
        var _totalCount: Int = 0;
        
        for (i in 0...5000000) {
            _numberOfRolls++;
            _totalCount += simulateSequence(thresholdForSecond, thresholdForThird);
        }
        
        return _totalCount / _numberOfRolls;
    }
    
    function traceResult (thresholdForSecond: Int = 0, thresholdForThird: Int = 0) {
        trace(thresholdForSecond + "/" + thresholdForThird + ": " + simulateABunchOfRolls(thresholdForSecond, thresholdForThird));
    }
}